close all;
clear;
clc;

TankSystem2;

%% 3.1
syms alpha beta;
xiL = [s+alpha, 0; 0, s+beta];    %interaktorova matice
R = (xiL * F)\subs(xiL,s,0);   %matice prenos.fci primovaz.reg.(aprox.inv.maticoveho prenosu sys.)
Fyw = F*R; %matice prenosovych fci uzavreneho sys.
FRS1 = R/(eye(size(Fyw)) - Fyw);    %zpetnovazebni MIMO regulator
FR1 = SymToTf(subs(FRS1, [alpha,beta], [0.0195,0.028]));

% 2. rad
syms alpha1 alpha2 beta1 beta2;
xiL2 = [s^2 + alpha1*s + alpha2, 0; 0, s^2 + beta1*s + beta2];
R = (xiL2 * F)\subs(xiL2,s,0);
Fyw = F*R; %matice prenosovych fci uzavreneho sys.
FRS2 = R/(eye(size(Fyw)) - Fyw);    %zpetnovazebni MIMO regulator
FR2 = SymToTf(subs(FRS2, [alpha1,alpha2,beta1,beta2], [2.6,0.05,0.5,0.015]));


%% 3.2
%{
%porovnani systemu
FR = FR1;
te1 = 0;
te2 = 0;
ae = 0;
np = 0;

sim('s3_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 7:8]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with 1 order regulator';

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with 1 order regulator - control';
%}

% 2. rad
%{

%porovnani systemu
FR = FR2;
te1 = 0;
te2 = 0;
ae = 0;
np = 0;
sim('s3_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 7:8]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system 2 order regulator';

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system 2 order regulator - control';
%}

%% 3.3

%{
%porovnani systemu
FR = FR1;
te1 = 500;
te2 = 800;
ae = 5;
np = 0;

sim('s3_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 7:8]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with 1 order regulator';

figure;
plot(simout.Time,simout.Data(:,[1:2 3:4 7:8]));
legend('w1', 'w2', 'di1', 'di2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with 1 order regulator - input error';

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with 1 order regulator - control';
%}

% 2. rad
%{
%porovnani systemu
FR = FR2;
te1 = 500;
te2 = 800;
ae = 5;
np = 0;
sim('s3_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 7:8]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system 2 order regulator';

figure;
plot(simout.Time,simout.Data(:,[1:2 3:4 7:8]));
legend('w1', 'w2', 'di1', 'di2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with 1 order regulator - input error';

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system 2 order regulator - control';
%}

%% 3.4

%{
figure;
sigma(FR1);
figure;
sigma(FR2);

% rad minimalni realizace systemu
size(ss(FR1, 'min').a,1)
size(ss(FR2, 'min').a,1)
%}

%% 3.5

%{
%porovnani systemu
FR = FR1;
te1 = 0;
te2 = 0;
ae = 0;
np = [0.01 0.01];

sim('s3_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 7:8]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with 1 order regulator';

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with 1 order regulator - control';
%}

% 2. rad
%{
%porovnani systemu
FR = FR2;
te1 = 0;
te2 = 0;
ae = 0;
np = [0.01 0.01];
sim('s3_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 7:8]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system 2 order regulator';

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system 2 order regulator - control';
%}