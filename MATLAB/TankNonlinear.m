function [dydt] = TankNonlinear( t, y, uRelative, u0, gamma1, gamma2 )
    TankParameters;

    u = uRelative(:,floor(t)+1) + u0';
    
    dh1 = -(a1/A1)*sqrt(2*g*y(1)) + (a3/A1)*sqrt(2*g*y(3)) + (gamma1*k1*u(1))/A1;
    dh2 = -(a2/A2)*sqrt(2*g*y(2)) + (a4/A2)*sqrt(2*g*y(4)) + (gamma2*k2*u(2))/A2;
    dh3 = -(a3/A3)*sqrt(2*g*y(3)) + ((1-gamma2)*k2*u(2))/A3;
    dh4 = -(a4/A4)*sqrt(2*g*y(4)) + ((1-gamma1)*k1*u(1))/A4;
    
    
    dydt = [dh1;dh2;dh3;dh4];
end
