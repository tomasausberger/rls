close all;
clear;
clc;

TankSystem2;

%% h2 optimal
% {
wM=2;
wA=0.0001;
wb=1;

w1=(p/wM+wb)/(p+wb*wA);

figure;
sigma(1/w1);
legend('1/w1');

w2 = 0.5;

W1 = [w1 0; 0 w1];
W2 = [w2 0; 0 w2];
W3 = [];

P=augw(FS,W1,W2,W3);

[K, CL] = h2syn(P);

L=FS*K;
I=eye(size(L));
S=inv(I+L);
T=I-S;
CS=K*S;

figure;
sigma(S,T,CS,K);
legend('S', 'T', 'CS', 'K');

% pridani integracni slozky
pol=min(abs(pole(K))); % nalezeni polu s nejmensim radiusem blizko imaginarni osy
K2=minreal(K*((p+pol)/p)); % a jeho odstraneni

%porovnani systemu
sim('s5_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 13:16]));
legend('w1', 'w2', 'y1_1', 'y2_1', 'y1_2', 'y2_2');
xlabel 't(s)';
title 'Response of system without (y_1) and with integration (y_2)';

figure;
plot(simout.Time,simout.Data(:,9:12));
legend('e1_1', 'e2_1', 'e1_2', 'e2_2');
xlabel 't(s)';
title 'Response of system without (e_1) and with integration (e_2) - error';

figure;
plot(simout.Time,simout.Data(:,5:8));
legend('u1_1', 'u2_1', 'u1_2', 'u2_2');
xlabel 't(s)';
title 'Response of system without (u_1) and with integration (u_2) - control';



L2=FS*K2;
I=eye(size(L2));
S2=inv(I+L2);
T2=I-S2;
CS2=K2*S2;

figure;
sigma(S2,T2,CS2,K2);
legend('S2', 'T2', 'CS2', 'K2');

K_H2=minreal(K2);

%}

%% h-inf
%{
wM=2;
wA=0.0001;
wb=0.05;

w1=(p/wM+wb)/(p+wb*wA);

figure;
sigma(1/w1);
legend('1/w1');

w2=0.05;

W1 = [w1 0; 0 w1];
W2 = [w2 0; 0 w2];
W3 = [];

P=augw(FS,W1,W2,W3);

[K,CL,GAM,INFO] = hinfsyn(P);

L=FS*K;
I=eye(size(L));
S=inv(I+L);
T=I-S;
CS=K*S;

figure;
sigma(S,'r',GAM/w1,'r--',T,'b',CS,'g',K,'m',ss(GAM/w2),'g--');
legend('S', 'H\infty cost/w1', 'T', 'CS', 'K', 'H\infty cost/w2');

% upravit pol na integrator
pol=min(abs(pole(K)));
K2=minreal(K*((p+pol)/p));

%porovnani systemu
sim('s5_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 13:16]));
legend('w1', 'w2', 'y1_1', 'y2_1', 'y1_2', 'y2_2');
xlabel 't(s)';
title 'Response of system without (y_1) and with integration (y_2)';

figure;
plot(simout.Time,simout.Data(:,9:12));
legend('e1_1', 'e2_1', 'e1_2', 'e2_2');
xlabel 't(s)';
title 'Response of system without (e_1) and with integration (e_2) - error';

figure;
plot(simout.Time,simout.Data(:,5:8));
legend('u1_1', 'u2_1', 'u1_2', 'u2_2');
xlabel 't(s)';
title 'Response of system without (u_1) and with integration (u_2) - control';

L2=FS*K2;
I=eye(size(L2));
S2=inv(I+L2);
T2=I-S2;
CS2=K2*S2;

figure;
sigma(S,'r',GAM/w1,'r--',T,'b',CS,'g',K,'m',ss(GAM/w2),'g--');
legend('S', 'H\infty cost/w1', 'T', 'CS', 'K', 'H\infty cost/w2');

K_Hinf=minreal(K2);

%}

%% 4 - 2 channels
%% h2 optimal
%{
wM1=2;
wA1=0.0001;
wb1=2;
wM2=2;
wA2=0.0001;
wb2=0.3;

w11=(p/wM1+wb1)/(p+wb1*wA1);
w12=(p/wM2+wb2)/(p+wb2*wA2);

figure;
hold on;
sigma(1/w11);
sigma(1/w12);
legend('1/w11','1/w12');

w2 = 0.05;

W1 = [w11 0; 0 w12];
W2 = [w2 0; 0 w2];
W3 = [];

P=augw(FS,W1,W2,W3);

[K, CL] = h2syn(P);

L=FS*K;
I=eye(size(L));
S=inv(I+L);
T=I-S;
CS=K*S;

figure;
sigma(S,T,CS,K);
legend('S', 'T', 'CS', 'K');

% pridani integracni slozky
pol=min(abs(pole(K))); % nalezeni polu s nejmensim radiusem blizko imaginarni osy
K2=minreal(K*((p+pol)/p)); % a jeho odstraneni

%porovnani systemu
sim('s5_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 13:16]));
legend('w1', 'w2', 'y1_1', 'y2_1', 'y1_2', 'y2_2');
xlabel 't(s)';
title 'Response of system without (y_1) and with integration (y_2)';

figure;
plot(simout.Time,simout.Data(:,9:12));
legend('e1_1', 'e2_1', 'e1_2', 'e2_2');
xlabel 't(s)';
title 'Response of system without (e_1) and with integration (e_2) - error';

figure;
plot(simout.Time,simout.Data(:,5:8));
legend('u1_1', 'u2_1', 'u1_2', 'u2_2');
xlabel 't(s)';
title 'Response of system without (u_1) and with integration (u_2) - control';



L2=FS*K2;
I=eye(size(L2));
S2=inv(I+L2);
T2=I-S2;
CS2=K2*S2;

figure;
sigma(S2,T2,CS2,K2);
legend('S2', 'T2', 'CS2', 'K2');

KD_H2=minreal(K2);

%}

%% h-inf
%{
wM1=2;
wA1=0.0001;
wb1=3.8;
wM2=2;
wA2=0.0001;
wb2=1;

w11=(p/wM1+wb1)/(p+wb1*wA1);
w12=(p/wM2+wb2)/(p+wb2*wA2);

figure;
hold on;
sigma(1/w11);
sigma(1/w12);
legend('1/w11','1/w12');

w2 = 0.5;

W1 = [w11 0; 0 w12];
W2 = [w2 0; 0 w2];
W3 = [];

P=augw(FS,W1,W2,W3);

[K,CL,GAM,INFO] = hinfsyn(P);

L=FS*K;
I=eye(size(L));
S=inv(I+L);
T=I-S;
CS=K*S;

figure;
sigma(S,'r',GAM/w11,'r--',GAM/w12,'r-.',T,'b',CS,'g',K,'m',ss(GAM/w2),'g--');
legend('S', 'H\infty cost/w11', 'H\infty cost/w12', 'T', 'CS', 'K', 'H\infty cost/w2');

% upravit pol na integrator
pol=min(abs(pole(K)));
K2=minreal(K*((p+pol)/p));

%porovnani systemu
sim('s5_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 13:16]));
legend('w1', 'w2', 'y1_1', 'y2_1', 'y1_2', 'y2_2');
xlabel 't(s)';
title 'Response of system without (y_1) and with integration (y_2)';

figure;
plot(simout.Time,simout.Data(:,9:12));
legend('e1_1', 'e2_1', 'e1_2', 'e2_2');
xlabel 't(s)';
title 'Response of system without (e_1) and with integration (e_2) - error';

figure;
plot(simout.Time,simout.Data(:,5:8));
legend('u1_1', 'u2_1', 'u1_2', 'u2_2');
xlabel 't(s)';
title 'Response of system without (u_1) and with integration (u_2) - control';

L2=FS*K2;
I=eye(size(L2));
S2=inv(I+L2);
T2=I-S2;
CS2=K2*S2;

figure;
sigma(S,'r',GAM/w11,'r--',GAM/w12,'r-.',T,'b',CS,'g',K,'m',ss(GAM/w2),'g--');
legend('S', 'H\infty cost/w11', 'H\infty cost/w12', 'T', 'CS', 'K', 'H\infty cost/w2');

KD_Hinf=minreal(K2);

%}