function [h0,u0] = TankLinearization(h10, h20, gamma1, gamma2)
    TankParameters;
    syms h30 h40 u10 u20;
    dh1 = -(a1/A1)*sqrt(2*g*h10) + (a3/A1)*sqrt(2*g*h30) + (gamma1*k1*u10)/A1;
    dh2 = -(a2/A2)*sqrt(2*g*h20) + (a4/A2)*sqrt(2*g*h40) + (gamma2*k2*u20)/A2;
    dh3 = -(a3/A3)*sqrt(2*g*h30) + ((1-gamma2)*k2*u20)/A3;
    dh4 = -(a4/A4)*sqrt(2*g*h40) + ((1-gamma1)*k1*u10)/A4;
    x = solve(dh1,dh2,dh3,dh4);
    h0 = [h10, h20, double(x.h30), double(x.h40)];
    u0 = [double(x.u10), double(x.u20)];
end