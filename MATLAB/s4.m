close all;
clear;
clc;

TankSystem;

Kpo = 0.1;
Kio = 0.5;
PIo1 = pid(Kpo,Kio);
PIo2 = pid(Kpo,Kio);

%% 4.1
Kstat = double(subs(F,s,0));
RGA_1 = Kstat.*inv(Kstat)';

FSI =[FS(1,2) FS(2,1); FS(2,2) FS(2,1)];
FI =[F(1,2) F(2,1); F(2,2) F(2,1)];
%{
FSS = FSI;
FR = [PIo1 0; 0 PIo2];
R = eye(2);

tmax = 600;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with switched IO and original PI';

figure;
plot(simout.Time,simout.Data(:,3:4));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with switched IO and original PI - control';
%}

%{
% PI reg.
Kpn1 = 35;
Kin1 = 0.8;
PIn1 = pid(Kpn1,Kin1);
Kpn2 = 35;
Kin2 = 0.9;
PIn2 = pid(Kpn2,Kin2);

FSS = FSI;
FR = [PIn1 0; 0 PIn2];
R = eye(2);

tmax = 600;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with switched IO and new PI';

figure;
plot(simout.Time,simout.Data(:,3:4));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with switched IO and new PI - control';
%}

%% 4.2

R_4_2 = inv(Kstat); %Fs(0)
R_4_2i = inv(double(subs(FI,s,0)));

%{
FSS = FS;
FR = [PIo1 0; 0 PIo2];
R = R_4_2;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with original PI';
%}

% funkcni:

Kpm = 0.01;
Kim = 0.005;
PIm1 = pid(Kpm,Kim);
PIm2 = pid(Kpm,Kim);

%{
FSS = FS;
FR = [PIm1 0; 0 PIm2];
R = R_4_2;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with modified PI';
%}

%{
FSS = FSI;
FR = [PIm1 0; 0 PIm2];
R = R_4_2i;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with switched IO and modified PI';
%}

%% 4.3
TankSystem2;

Kstat = double(subs(F,s,0));
RGA_3 = Kstat.*inv(Kstat)';

%% 4.4
syms alpha beta;
xiL = [s+alpha, 0; 0, s+beta];    %interaktorova matice
Rz = (xiL * F)\subs(xiL,s,0);

alpha = -1/pole(FS(1,1));
beta = -1/pole(FS(2,2));
R_4_4 = minreal(SymToTf(subs(Rz)));

%{
FSS = FS;
FR = [PIo1 0; 0 PIo2];
R = R_4_4;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with decoupler';
%}

%% 4.5

R_4_5 = [FS(2,2), -FS(1,2); -FS(2,1), FS(1,1)];
%{
FSS = FS;
FR = eye(2);
R = R_4_5;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('y1', 'y2');
xlabel 't(s)';
title 'Response of system with decoupling regulator';
%}

%% 4.6

K = minreal(FS(1,2)*FS(2,1) / (FS(1,1)*FS(2,2)));
K_bar = 1-K;
R_4_6 = minreal([K_bar^-1, -(K_bar^(-1))*(FS(1,2)/FS(1,1));
    -(K_bar^(-1))*(FS(2,1)/FS(2,2)), K_bar^-1]);

%{
FSS = FS;
FR = eye(2);
R = R_4_6;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,5:6));
legend('y1', 'y2');
xlabel 't(s)';
title 'Response of system with decoupling regulator';
%}

%% 4.7

%{
figure;
sigma(R_4_4, 'b', R_4_5, 'r', R_4_6, 'm');
legend('R 4.4', 'R 4.5', 'R 4.6');
%}

%% 4.8
% FS*R_4_5
%[7.642e-08,8.006e-08,1.218e-08,3.14e-10,2.234e-12]
%[0.262,0.2824,0.06357,0.003213,6.807e-05,6.613e-07,2.441e-09]

Kp = 1209.9;
Ki = 11.150;
PI_4_5 = pid(Kp,Ki);

%{
FSS = FS;
FR = [PI_4_5 0; 0 PI_4_5];
R = R_4_5;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with R 4.5';

figure;
plot(simout.Time,simout.Data(:,3:4));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with R 4.5 - control';
%}

% pro ukol 6, Q-parametrizace
tau = 52;
filtr = 1/(tau*p+1);
Q = filtr/FS(1,1);
C = minreal(Q/(1-Q*FS(1,1)));
PI1_4_6 = pid(C);

tau = 37;
filtr = 1/(tau*p+1);
Q = filtr/FS(2,2);
C = minreal(Q/(1-Q*FS(2,2)));
PI2_4_6 = pid(C);

%{
FSS = FS;
FR = [PI1_4_6 0; 0 PI1_4_6];
R = R_4_6;

tmax = 1200;
te1 = 0;
te2 = 0;
ae = 0;
sim('s4_mdl');

figure;
plot(simout.Time,simout.Data(:,[1:2 5:6]));
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with R 4.6';

figure;
plot(simout.Time,simout.Data(:,3:4));
legend('u1', 'u2');
xlabel 't(s)';
title 'Response of system with R 4.6 - control';
%}