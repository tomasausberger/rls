close all;
clear;
clc;

TankSystem2;

%% generate data
%{
% generate models - wait for it (need enough memory)
count = 200;
[h0r, u0r] = TankRandom(gamma1,gamma2, 0.2, 2, 0.2, 2, count);
%frekvence pro vysetreni multiplik. perturbace
w=logspace(-3,3,2000);
wo=zeros(size(w));
syms h10 h20 h30 h40;

Grs = sym(zeros(2,2,count));
Grtf = tf(zeros(2,2,count));
for i=1:count
    Grs(:,:,i) = subs(subs(F_sym),[h10,h20,h30,h40],h0r(:,i)');
    Grtf(:,:,i) = SymToTf(Grs(:,:,i));
end;

Gn = subs(F_sym);
for k=1:length(w)
    wk = w(k)
    %Nominalni model
    G = double(subs(Gn,[s,h10,h20,h30,h40],[wk*1j,h0]));
    
    sig=zeros(1,count);
    for i=1:count
        Gp = double(subs(Grs(:,:,i),s,wk*1j));
        
        delta=(Gp-G)*pinv(G);
        sig(i)=norm(delta); %max. singularni cislo relativni chyby
    end;
    wo(k)=max(sig); %nejvetsi chyba pres vsechny nahodne realizace
end;
save('s6.mat','wo','w','h0r','Grtf');
%}
% {
load('s6.mat');
%}

%% hinf
wM=2;
wA=0.0001;
wb=0.05;

w1=(p/wM+wb)/(p+wb*wA);
w2=0.05;

W1 = [w1 0; 0 w1];
W2 = [w2 0; 0 w2];
W3 = [];
P=augw(FS,W1,W2,W3);

[K,CL,GAM] = hinfsyn(P);
% upravit pol na integrator
pol=min(abs(pole(K)));
K_Hinf=minreal(K*((p+pol)/p));

L=FS*K_Hinf;
I=eye(size(L));
S=inv(I+L);
T=I-S;

%% 6.1
uM = 50;
uA = 1.5;
uw = 80;
wos = (p/uM+uw)/(p+uw*uA);

%{
figure;
loglog(w,sigma(wos,w),'b');
hold on;
loglog(w,wo,'r');
legend('wo','max perturbation singular value');
title('Multiplicative uncertainity');
ylabel('Singular values (B)');
xlabel('Frequency (rad/s)');
%}

% min(wos-wo) > 0 -> multiplikativni neurcitost (wos > wo)
min(sigma(wos,w)-wo);

%% 6.2
% norm < 1 -> robustni stabilita
norm(T*wos,inf);

%{
figure;
loglog(w,sigma(T*wos,w),'r');
legend('wo*T');
title('Robust stability - uncertainity');
ylabel('Singular values (B)');
xlabel('Frequency (rad/s)');
%SYSTEM JE ROBUSTNE STABILNI PRO VSECHNY PERTURBACE pokud ||wo*T||<1
%}

%% 6.3
% norm < 1 -> nominalni kvalita rizeni
norm(W1*S/GAM,inf);

% max(sigma(W1*S/GAM)+sigma(wos*T)) < 1 -> robustni kvalita rizeni (nevyjde)
max(max(sigma(W1*S/GAM,w)+sigma(wos*T,w)));

%% 6.4
% modifikujeme W1 - slevujeme z kvality pro dosahnuti robustnosti (menime wb, nez dosahneme robustnosti)
wMr=2;
wAr=0.0001;
wbr=0.026;

w1r=(p/wMr+wbr)/(p+wbr*wAr);
W1r = [w1r 0; 0 w1r];

% max(sigma(W1*S/GAM)+sigma(wos*T)) < 1 -> robustni kvalita rizeni
max(max(sigma(W1r*S/GAM,w)+sigma(wos*T,w)));

%{
figure;
pl1 = loglog(w, sigma(1/W1,w), 'r');
hold on;
pl2 = loglog(w, sigma(1/W1r,w), 'm');
pl3 = loglog(w,sigma(S*wos,w),'b');
legend([pl1(1) pl2(1) pl3(1)], {'1/w1 - nominal','1/w1 - robust','wo*S'});
title('Robust quality control - uncertainity');
ylabel('Singular values (B)');
xlabel('Frequency (rad/s)');
%SYSTEM MA ROBUSTNI KVALITU RIZENI pokud ||wo*S||<1/W1
%}


%% 6.5
%{
tfcount = size(Grtf,3);
TWosigma = zeros(tfcount*2,size(w,2));
SWosigma = zeros(tfcount*2,size(w,2));
for i = 1:tfcount
    i
    Lt=Grtf(:,:,i)*K_Hinf;
    It=eye(size(Lt));
    St=inv(It+Lt);
    Tt=It-St;
    TWosigma([i i+tfcount],:) = sigma(wos*Tt,w);
    SWosigma([i i+tfcount],:) = sigma(wos*St,w);
end
save('s6.1.mat','TWosigma','SWosigma')
%}
% {
load('s6.1.mat');
%}

%{
figure;
loglog(w,TWosigma,'b');
legend('wo*T');
title('Robust stability - simulation');
ylabel('Singular values (B)');
xlabel('Frequency (rad/s)');
%SYSTEM JE ROBUSTNE STABILNI PRO VSECHNY PERTURBACE pokud ||wo*T||<1
%}

%{
figure;
pl1 = loglog(w, sigma(GAM/W1,w), 'r');
hold on;
pl2 = loglog(w, sigma(GAM/W1r,w), 'm');
pl3 = loglog(w, SWosigma,'b');
legend([pl1(1) pl2(1) pl3(1)], {'GAM/w1 - nominal','GAM/w1 - robust','wo*S'});
title('Robust quality control - simulation');
ylabel('Singular values (B)');
xlabel('Frequency (rad/s)');
%SYSTEM MA ROBUSTNI KVALITU RIZENI pokud ||wo*S||<1/W1
%}
