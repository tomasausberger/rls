syms A1 A2 A3 A4 a1 a2 a3 a4 gamma1 gamma2 k1 k2 g h10 h20 h30 h40 u10 u20

dh1 = -(a1/A1)*sqrt(2*g*h10) + (a3/A1)*sqrt(2*g*h30) + (gamma1*k1*u10)/A1;
dh2 = -(a2/A2)*sqrt(2*g*h20) + (a4/A2)*sqrt(2*g*h40) + (gamma2*k2*u20)/A2;
dh3 = -(a3/A3)*sqrt(2*g*h30) + ((1-gamma2)*k2*u20)/A3;
dh4 = -(a4/A4)*sqrt(2*g*h40) + ((1-gamma1)*k1*u10)/A4;


h0 = [h10;h20;h30;h40];
df = [dh1;dh2;dh3;dh4];
u0 = [u10;u20];
A = jacobian(df,h0);
B = jacobian(df,u0);

%% ss
syms T1 T2 T3 T4
%substituce Ti = (Ai/ai)*sqrt((2*hi0)/g)
A_subs = [-1/T1 0 A3/(A1*T3) 0;
    0 -1/T2 0 A4/(A2*T4);
    0 0 -1/T3 0;
    0 0 0 -1/T4];
C = [1 0 0 0;
    0 1 0 0];
D = zeros(2);

%% tf
p = tf('p');
syms s;
F_sym = C * inv(s*eye(size(A)) - A) * B;
Fs = C * inv(s*eye(size(A_subs)) - A_subs) * B;


%% Smith-McMillan

syms s;
S = [A2*T1*k1*gamma1*(T2*s+1)*(T3*s+1)*(T4*s+1), A2*T1*k2*(1-gamma2)*(T2*s+1)*(T4*s+1);
    A1*T2*k1*(1-gamma1)*(T1*s+1)*(T3*s+1), A1*T2*k2*gamma2*(T1*s+1)*(T3*s+1)*(T4*s+1)];
factor(det(S));

%% neurcitost
%{
Ts1 = (A1/a1)*sqrt((2*h10)/g);
Ts2 = (A2/a2)*sqrt((2*h20)/g);
Ts3 = (A3/a3)*sqrt((2*h30)/g);
Ts4 = (A4/a4)*sqrt((2*h40)/g);

syms wk;
Ps11=(gamma1*k1)/A1*Ts1/(Ts1*(j*wk)+1);
Ps12=((1-gamma2)*k2)/A1*Ts1/((Ts1*(j*wk)+1)*(Ts3*(j*wk)+1));
Ps21=((1-gamma1)*k2)/A2*Ts2/((Ts2*(j*wk)+1)*(Ts4*(j*wk)+1));
Ps22=(gamma2*k2)/A2*Ts2/(Ts2*(j*wk)+1);

Gs=[Ps11 Ps12;Ps21 Ps22];
%}