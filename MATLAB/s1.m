close all;
clear;
clc;

%% dosazeni ustaleneho bodu

TankSystem;

%% srovnani

%{
tmax = 200;
t = 0:tmax;
us = zeros(2, tmax+1);
us(1,10:tmax+1) = 1;

ode = ode45(@(t,y) TankNonlinear( t, y, us, u0, gamma1, gamma2), [0, tmax], [h10,h20,h30,h40], odeset('MaxStep',1));
nl_t = ode.x;
nl_y = zeros(4,size(nl_t,2));
nl_u = zeros(2,size(nl_t,2));
for i = 1:size(nl_t,2)
    nl_u(:,i) = us(:,floor(nl_t(i))+1);
    nl_y(:,i) = ode.y(:,i) - [h10;h20;h30;h40];
end

figure;
hold on;
title('Nonlinear system - step u1');
plot(nl_t,nl_u(1,:),'b');
plot(nl_t,nl_u(2,:),'c');
plot(nl_t,nl_y(1,:),'r');
plot(nl_t,nl_y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';


ss_y = lsim(SS, us, t)';
figure;
hold on;
title('Linear system SS - step u1');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,ss_y(1,:),'r');
plot(t,ss_y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';

tf_y = lsim(FS, us, t)';
figure;
hold on;
title('Linear system TF - step u1');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,tf_y(1,:),'r');
plot(t,tf_y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';

us = zeros(2, tmax+1);
us(2,10:tmax+1) = 1;

ode = ode45(@(t,y) TankNonlinear(  t, y, us, u0, gamma1, gamma2), [0, tmax], [h10,h20,h30,h40], odeset('MaxStep',1));
nl_t = ode.x;
nl_y = zeros(4,size(nl_t,2));
nl_u = zeros(2,size(nl_t,2));
for i = 1:size(nl_t,2)
    nl_u(:,i) = us(:,floor(nl_t(i))+1);
    nl_y(:,i) = ode.y(:,i) - [h10;h20;h30;h40];
end

figure;
hold on;
title('Nonlinear system - step u2');
plot(nl_t,nl_u(1,:),'b');
plot(nl_t,nl_u(2,:),'c');
plot(nl_t,nl_y(1,:),'r');
plot(nl_t,nl_y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';


ss_y = lsim(SS, us, t)';
figure;
hold on;
title('Linear system SS - step u2');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,ss_y(1,:),'r');
plot(t,ss_y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';

tf_y = lsim(FS, us, t)';
figure;
hold on;
title('Linear system TF - step u2');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,tf_y(1,:),'r');
plot(t,tf_y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';
%}


%% poly
% ze stav. repre.
ps = eig(A);
% z matice prenosu
det_F = det(F);
[num,den] = numden(det_F);
solve(den);

%% nuly
% ze stav. repre. (z Rosenbrockovy matice)
P = [p*eye(size(A)) - A, -B; C, D];
ssz = tzero(P);
%tzero(A,B,C,D)

% z matice prenosu
tfz = double(solve(num));



% vstupni smer nul
z = ssz;
zv = zeros(2);
zw = zeros(2);
for i = 1 : length(z)
    syms v1 v2
    v = [v1; v2];
    V = subs(F,z(i))*v;
    v11 = solve(V(1,1));
    v12 = solve(V(2,1));
    v2 = 1;
    v1 = (double(subs(v11)) + double(subs(v12))) /2;
    v = double(subs(v));
    zv(i,:) = v/norm(v);
end

% vystupni smer nul
for i = 1 : length(z)
    syms w1 w2
    w = [w1, w2];
    W = w * subs(F,z(i));
    w11 = solve(W(1,1));
    w12 = solve(W(1,2));
    w2 = 1;
    w1 = (double(subs(w11)) + double(subs(w12))) /2;
    w = double(subs(w));
    zw(i,:) = w/norm(w);
end


%% blokovani prenosu nestabilni nuly

tmax = 70;
t = 0:tmax;
us1 = zeros(2, tmax+1);
us2 = zeros(2, tmax+1);
e = exp(1);
for i = 1:tmax+1
    us1(:,i) = (zv(1,:)*e^(z(1)*t(i)))';
    us2(:,i) = (zv(2,:)*e^(z(2)*t(i)))';
end


y = lsim(FS, us1, t)';
figure
hold on
title('Transfer block - z1');
plot(t,us1(1,:),'b');
plot(t,us1(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';

y = lsim(FS, us2, t)';
figure
hold on
title('Transfer block - z2');
plot(t,us2(1,:),'b');
plot(t,us2(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';



%% Smith-McMillan

%substituce Ti = (Ai/ai)*sqrt((2*hi0)/g)
T1 = (A1/a1)*sqrt((2*h10)/g);
T2 = (A2/a2)*sqrt((2*h20)/g);
T3 = (A3/a3)*sqrt((2*h30)/g);
T4 = (A4/a4)*sqrt((2*h40)/g);

syms s
S = [A2*T1*k1*gamma1*(T2*s+1)*(T3*s+1)*(T4*s+1), A2*T1*k2*(1-gamma2)*(T2*s+1)*(T4*s+1);
    A1*T2*k1*(1-gamma1)*(T1*s+1)*(T3*s+1), A1*T2*k2*gamma2*(T1*s+1)*(T3*s+1)*(T4*s+1)];
factor(det(S));

%% riditelnost, pozorovatelnost

CO = ctrb(A,B);
OB = obsv(A,C);
rank(CO);
rank(OB);

%% PI reg.

%{
Kp = 0.1;
Ki = 0.5;
PI = pid(Kp,Ki);

tmax = 600;
t = 0:tmax;
us = zeros(2, tmax+1);
us(1,10:tmax+1) = 1;
y = lsim(feedback(FS*[PI 0; 0 1],[1 0; 0 0]), us, t)';

figure;
hold on;
title('Step u1 - loop 1');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';


us = zeros(2, tmax+1);
us(2,10:tmax+1) = 1;
y = lsim(feedback(FS*[1 0; 0 PI],[0 0; 0 1]), us, t)';

figure;
hold on;
title('Step u2 - loop 2');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';


y = lsim(feedback(FS*[PI 0; 0 PI],[1 0; 0 1]), us, t)';

figure;
hold on;
title('Step u2 - two loops');
plot(t,us(1,:),'b');
plot(t,us(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2', 'y1', 'y2');
xlabel 't(s)';


Az = [A1*(T1*p+1)*(T3*p+1)*p, 0; 0, A2*(T2*p+1)*(T4*p+1)*p] +...
    [A1*(T1*p+1)*(T3*p+1)*p, 0; 0, A2*(T2*p+1)*(T4*p+1)*p] +...
    [T1*k1*gamma1*(T3*p+1), T1*k2*(1-gamma2); T2*k1*(1-gamma1), T2*k2*gamma2*(T4*p+1)]*[Kp*p+Ki, 0; 0, Kp*p+Ki];
tzero(Az);

%}