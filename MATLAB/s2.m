close all;
clear;
clc;

TankSystem;

%% state regulator
K = place(A,B, [-0.06, -0.05, -0.06, -0.05]);
S = ss(A-B*K,B,C,D);

%{
figure;
hold on;
tmax = 300;
t = 0:tmax;
w = zeros(2,tmax+1);
w(:,1:tmax+1) = 0.1;
y = lsim(S, w, t)';
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'y1', 'y2');
ylabel 'y(t)';
xlabel 't';
title 'Step response of the closed system';

SS = ss(A-B*K,B,-K,eye(2));
figure;
hold on;
y = lsim(SS, w, t)';
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2');
ylabel 'u(t)';
xlabel 't';
title 'Step response of the closed system - control';
%}

%% static gain
Ks = C*(inv(-A+B*K))*B;     %staticke zesileni
Kk = inv(Ks);
SC = ss(A-B*K,B*Kk,C,D);
x = (p*eye(size(A)) - A)\ B;

%{
figure;
hold on;
tmax = 300;
t = 0:tmax;
w = zeros(2,tmax+1);
w(:,1:tmax+1) = 0.1;
y = lsim(SC, w, t)';
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'y1', 'y2');
ylabel 'y(t)';
xlabel 't';
title 'Step response of system with compensated static gain';

SCS = ss(A-B*K,B*Kk,-K,Kk);
figure;
hold on;
y = lsim(SCS, w, t)';
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('u1', 'u2');
ylabel 'u(t)';
xlabel 't';
title 'Step response of system with compensated static gain - control';
%}

%{
sim('s2_state_reg_error');

t = simout.Time;
u = simout.Data(:,1:2)';
di = simout.Data(:,3:4)';
w = simout.Data(:,5:6)';
y = simout.Data(:,7:8)';
figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,di(1,:),'y');
plot(t,di(2,:),'g');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'di1', 'di2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with input error';

figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,di(1,:),'y');
plot(t,di(2,:),'g');
plot(t,u(1,:),'r');
plot(t,u(2,:),'m');
legend('w1', 'w2', 'di1', 'di2', 'u1', 'u2');
xlabel 't(s)';
title 'Response of system with input error - control';
%}

%% regulator with integration (2.3)

%{
A_i = [A,zeros(4,2);-C,zeros(2,2)];
B_i = [B;zeros(2,2)];
P_i = [-0.08, -0.08, -0.05, -0.05, -0.12, -0.12];
K_i = place(A_i,B_i,P_i);

sim('s2_state_reg_i_error');

t = simout.Time;
u = simout.Data(:,1:2)';
di = simout.Data(:,3:4)';
w = simout.Data(:,5:6)';
y = simout.Data(:,7:8)';
figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,di(1,:),'y');
plot(t,di(2,:),'g');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'di1', 'di2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with integration';

figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,di(1,:),'y');
plot(t,di(2,:),'g');
plot(t,u(1,:),'r');
plot(t,u(2,:),'m');
legend('w1', 'w2', 'di1', 'di2', 'u1', 'u2');
xlabel 't(s)';
title 'Response of system with integration - control';
%}

%% 2.4

Crek = [C;0 0 0 0;0 0 0 0];
D = [0, 0; 0, 0];
Prek = [-0.28, -0.28, -0.159, -0.222];
%Prek = [-0.28, -0.28, -0.14, -0.4];
Krek = place(A',Crek',Prek);

%{
A_i = [A,zeros(4,2);-C,zeros(2,2)];
B_i = [B;zeros(2,2)];
P_i = [-0.08, -0.08, -0.05, -0.05, -0.12, -0.12];
K_i = place(A_i,B_i,P_i);

sim('s2_state_reg_i_rek');

t = simout.Time;
u = simout.Data(:,1:2)';
di = simout.Data(:,3:4)';
w = simout.Data(:,5:6)';
y = simout.Data(:,7:8)';
figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,di(1,:),'y');
plot(t,di(2,:),'g');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'di1', 'di2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with reconstructor';

figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,di(1,:),'y');
plot(t,di(2,:),'g');
plot(t,u(1,:),'r');
plot(t,u(2,:),'m');
legend('w1', 'w2', 'di1', 'di2', 'u1', 'u2');
xlabel 't(s)';
title 'Response of system with reconstructor - control';
%}

%% 2.5

sigma2 = 0.01;

% alfa = 0.01^(-2);
% beta = 5^(-2);
% Q = 8*eye(4);
Q = [8 0 0 0;
    0 8 0 0;
    0 0 25 0;
    0 0 0 8];
R = 0.01*eye(2);
[KLQR,S,E] = lqr(A,B,Q,R);

KsLQR = C*(inv(-A+B*KLQR))*B;
KkLQR = inv(KsLQR);

QN = [1.5e6,0;0,1e6];
RN = sigma2*eye(2); %sigma patri sem
SYS = ss(A,B,C,0);
[KEST,L,P] = kalman(SYS,QN,RN);

%{
sim('s2_state_lq_reg');

t = simout.Time;
u = simout.Data(:,1:2)';
w = simout.Data(:,3:4)';
y = simout.Data(:,5:6)';
figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with LQ regulator and Kalman filter';

figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,u(1,:),'r');
plot(t,u(2,:),'m');
legend('w1', 'w2', 'u1', 'u2');
xlabel 't(s)';
title 'Response of system with LQ regulator and Kalman filter - control';
%}


%% 2.6

%KJFA = [-152.7838771, 280.0919270; -620.9080287, 1128.754908];
KJFA = [837.3067213, 261.8418828; -612.4331097, -191.3657578];

KsJFA = C*(inv(-A-B*KJFA*C))*B; %pripadne -A+BKC a otocit znaminko v modelu
KkJFA = inv(KsJFA);


% {
sim('s2_jfa');

t = simout.Time;
u = simout.Data(:,1:2)';
w = simout.Data(:,3:4)';
y = simout.Data(:,5:6)';
figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,y(1,:),'r');
plot(t,y(2,:),'m');
legend('w1', 'w2', 'y1', 'y2');
xlabel 't(s)';
title 'Response of system with static output feedback';

figure;
hold on;
plot(t,w(1,:),'b');
plot(t,w(2,:),'c');
plot(t,u(1,:),'r');
plot(t,u(2,:),'m');
legend('w1', 'w2', 'u1', 'u2');
xlabel 't(s)';
title 'Response of system with static output feedback - control';
%}