function [sysTf] = SymToTf(sysSym)

sysTf = tf(zeros(size(sysSym)));
[num,den] = numden(sysSym);

for r=1:size(sysSym,1)
    for c=1:size(sysSym,2)
        sysTf(r,c) = tf(double(sym2poly(num(r,c))),double(sym2poly(den(r,c))));
    end
end
end