function [h0,u0] = TankRandom(gamma1, gamma2, h10_l, h10_h, h20_l, h20_h, count)
gamma = [gamma1,gamma2];
Tank;    
TankParameters;
%overwrite
gamma1 = gamma(1);
gamma2 = gamma(2);

h0 = zeros(4,count);
u0 = zeros(2,count);

h10_rand = h10_l + (h10_h-h10_l).*rand(count,1);
h20_rand = h20_l + (h20_h-h20_l).*rand(count,1);

for i = 1:count
    h10 = h10_rand(i);
    h20 = h20_rand(i);
    
    p1 = gamma1/(1-gamma1);
    p2 = gamma2/(1-gamma2);
    t10 = a1*sqrt(2*g*h10);
    t20 = a2*sqrt(2*g*h20);
    h40 = (((t20 - (t10*p2))/(a4*(1-(p1*p2))))^2)/(2*g);
    t40 = a4*sqrt(2*g*h40);
    h30 = (((t10 - (t40*p1))/a3)^2)/(2*g);
    t30 = a3*sqrt(2*g*h30);
    u10 = t40/((1-gamma1)*k1);
    u20 = t30/((1-gamma2)*k2);
    
    h0(:,i) = [h10,h20,h30,h40];
    u0(:,i) = [u10,u20];
end
end