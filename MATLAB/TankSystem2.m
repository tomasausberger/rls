Tank;
TankParameters;

h10 = 1.6;
h20 = 1.4;
gamma1 = 0.6;
gamma2 = 0.7;
[h0,u0] = TankLinearization(h10, h20, gamma1, gamma2);
h30 = h0(3);
h40 = h0(4);
u10 = u0(1);
u20 = u0(2);

A = double(subs(A));
B = double(subs(B));
F = subs(F_sym);

FS = SymToTf(F);
SS = ss(A,B,C,D);